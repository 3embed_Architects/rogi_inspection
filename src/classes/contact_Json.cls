/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Encapsulates all service layer logic for a given function or module in the application
 * 
 * For more guidelines and details see 
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Service_Layer
 *
 **/
public class contact_Json
{

	public String cName{get;set;}
	public String cPhone{get;set;}
	public String cEmail{get;set;}
	public cOrganisation cOrganisation{get;set;}
	public cAddress cAddress{get;set;}
	public cShipTo cShipTo{get;set;}
	public cBillTo cBillTo{get;set;}

	public contact_Json (String fname,String lname,Id project_id,String phone,String mobile_no, String fax,String email,String street,String city,String state,String postal_Code,String country,Id project_related_member  )
	{
		
			
			this.cName = fname;
			this.cEmail = email;
			this.cPhone = phone;

			 cOrganisation =  new cOrganisation();
				cOrganisation.cId = '586b797eb608cb68f206f335';
				cOrganisation.cName = '3Embed';

			//cAddress

			  cAddress =  new cAddress();
				cAddress.line1 = street;
				cAddress.line2 = street;
				cAddress.city = city;
				cAddress.state = state;
				cAddress.country = country;
				cAddress.post_code = postal_Code;

			//shipto	

			 cShipTo = new cShipTo();
				cShipTo.name = fname;
				cShipTo.phone = phone;
				cShipTo.line1 = street;
				cShipTo.line2 = street;
				cShipTo.city = city;
				cShipTo.state = state;
				cShipTo.country = country;
				cShipTo.post_code =postal_Code;

			//billTo
			
			 cBillTo = new cBillTo();
				cBillTo.name = fname;
				cBillTo.phone = phone;
				cBillTo.line1 = street;
				cBillTo.line2 = street ;
				cBillTo.city = city;
				cBillTo.state = state;
				cBillTo.country = country;
				cBillTo.post_code =postal_Code;

	}

	
	public class cOrganisation{

		 String cId;
		 String cName;

	}
	public class cAddress{

		public String line1;
		public String line2;
		public String city;
		public String state;
		public String country;
		public String post_code;					
	}
	public class cShipTo{

		public String name;
		public String phone;
		public String line1;
		public String line2;
		public String city;
		public String state;
		public String country;
		public String post_code;
	}

	public class cBillTo{

		public String name;
		public String phone;
		public String line1;
		public String line2;
		public String city;
		public String state;
		public String country;
		public String post_code;

	}

}