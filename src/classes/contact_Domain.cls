/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Encapsulates all behaviour logic relating to the Contact object
 * 
 * For more guidelines and details see 
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
 *
 **/
public class contact_Domain extends fflib_SObjectDomain 
{
	public contact_Domain(List<Contact> records) 
	{
		super(records);	
	}

	public override void onValidate()
	{
		for(Contact record : (List<Contact>) Records)
		{
		}
	}

	public override void onValidate(Map<Id,SObject> existingRecords)
	{
		for(Contact record : (List<Contact>) Records)
		{
		}
	}

	public override void onAfterInsert()
	{
		for(Contact record : (List<Contact>) Records)
		{
			contact_Json  newCustomer = new contact_Json(record.FirstName,
				record.LastName,
				record.AccountId,
				record.Phone,
				record.MobilePhone,
				record.Fax,
				record.Email,
				record.MailingStreet,
				record.MailingCity,
				record.MailingState,
				record.MailingPostalCode,
				record.MailingCountry,
				record.Project_Related_Member_del__c);

				String jsonBody = JSON.serialize(newCustomer);
				system.debug(LoggingLevel.FINEST, 'KKKKKKKKKKKKKKKKKKKKKKK'+jsonBody);
				contactHttp.contactCallout(jsonBody);
		}
	}

	public void someMethod()
	{
		for(Contact record : (List<Contact>) Records)
		{
		}		
	}

	public class Constructor implements fflib_SObjectDomain.IConstructable2
	{
		public fflib_SObjectDomain construct(List<SObject> sObjectList)
		{
			return new contact_Domain(sObjectList);
		}

		public fflib_SObjectDomain construct(List<SObject> sObjectList, SObjectType sObjectType)
		{
			return new contact_Domain(sObjectList);
		}			
	}	
}