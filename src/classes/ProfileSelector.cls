/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */

/**
 * Class encapsulates query logic for Profile
 *
 * https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Selector_Layer
 **/
public class ProfileSelector extends fflib_SObjectSelector
{
	public List<Schema.SObjectField> getSObjectFieldList()
	{
		return new List<Schema.SObjectField> {
				Profile.Id,
				Profile.Name
			};
	}

	public Schema.SObjectType getSObjectType()
	{
		return Profile.sObjectType;
	}

	public List<Profile> selectById(Set<ID> idSet)
	{
		return (List<Profile>) selectSObjectsById(idSet);
	}	
	public List<SObject> adminProfile() {
		fflib_QueryFactory addPerMemQuery = newQueryFactory();
		return new List<SObject>(Database.query(addPerMemQuery.setCondition('Name=System Administrator').toSOQL()));
	}

	/*
	 * For more examples see https://github.com/financialforcedev/fflib-apex-common-samplecode
	 * 
	public List<Profile> selectBySomethingElse(List<String> somethings)
	{
		assertIsAccessible();
		return (List<Profile>) Database.query(
				String.format(
				'select {0}, ' +
				  'from {1} ' +
				  'where Something__c in :somethings ' + 
				  'order by {2}',
				new List<String> {
					getFieldListString(),
					getSObjectName(),
					getOrderBy() } ) );
	}
	 */
}