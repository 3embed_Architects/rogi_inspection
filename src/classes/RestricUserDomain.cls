/**
 * See https://github.com/financialforcedev/fflib-apex-common for more info
 *
 * Install library via 
 *   https://githubsfdeploy.herokuapp.com/app/githubdeploy/financialforcedev/fflib-apex-common
 */
/**
 * Encapsulates all behaviour logic relating to the Task object
 * 
 * For more guidelines and details see 
 *   https://developer.salesforce.com/page/Apex_Enterprise_Patterns_-_Domain_Layer
 *
 **/
public class RestricUserDomain extends fflib_SObjectDomain 
{
	Profile AdministratorProfile=[select id,Name from Profile where Name='System Administrator'];
	Profile managerProfile=[select id,Name from Profile where Name='Manager'];
	Id currentProfileId=UserInfo.getProfileId();
	public RestricUserDomain(List<Task> records) 
	{
		super(records);	
	}

	public override void onValidate()
	{
		for(Task record : (List<Task>) Records)
		{
		}
	}

	public override void onValidate(Map<Id,SObject> existingRecords)
	{

		for(Task record : (List<Task>) Records)
		{
			
		}
	}

	public override void onBeforeInsert()
	{
		for(Task record : (List<Task>) Records)
		{
			if(currentProfileId!=AdministratorProfile.Id && currentProfileId!= managerProfile.Id){
				record.addError('you have not access permission to Create Task');
			}
		}
	}
	
	public override void onBeforeDelete(){
		for(Task record : (List<Task>) Records)
		{
			if(currentProfileId!=AdministratorProfile.Id && currentProfileId!= managerProfile.Id){
				record.addError('you have not  access permission to delete Task');
			}
		}

	}

	public class Constructor implements fflib_SObjectDomain.IConstructable2
	{
		public fflib_SObjectDomain construct(List<SObject> sObjectList)
		{
			return new RestricUserDomain(sObjectList);
		}

		public fflib_SObjectDomain construct(List<SObject> sObjectList, SObjectType sObjectType)
		{
			return new RestricUserDomain(sObjectList);
		}			
	}	
}