@isTest
private class restricTaskTest {


	
    public static User createTestUser(Id roleId, Id profID, String fName, String lName)
    {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
       
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                                lastName = lName,
                                email = uniqueName + '@test' + orgId + '.org',
                                Username = uniqueName + '@test' + orgId + '.org',
                                EmailEncodingKey = 'ISO-8859-1',
                                Alias = uniqueName.substring(18, 23),
                                TimeZoneSidKey = 'America/Los_Angeles',
                                LocaleSidKey = 'en_US',
                                LanguageLocaleKey = 'en_US',
                                ProfileId = profId,
                                UserRoleId = roleId);
        return tuser;
    }

    
static testMethod void positiveDataInsert()
    {
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
       
        UserRole ur = new UserRole(Name = 'CEO');
        insert ur;
        User usr = createTestUser(ur.Id, pf.Id, 'Test FirstName', 'Test LastName');
       
        System.runAs(usr)
        {
            Task tsk=new Task();
            tsk.OwnerId=usr.id;
            tsk.Status='In Progress';
            tsk.Subject='Email';
            tsk.Priority='Normal';
            insert tsk;
        }
    }
    
    
}