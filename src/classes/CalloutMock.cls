//Callout Test CLASS
@isTest
public class CalloutMock implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/JSON');
        res.setBody('GREAT SCOTT');
        res.setStatusCode(200);
        res.setStatus('OK');
        return res;
    }

}