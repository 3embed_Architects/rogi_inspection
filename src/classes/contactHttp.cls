public with sharing class contactHttp {
	
		@Future(callout = true)
		public static void contactCallout(String jsonBody)
		{
				HttpRequest req = new HttpRequest();
				req.setEndpoint('http://159.203.166.180:4545/customer');
				req.setMethod('POST');
				req.setHeader('Authorization','Basic cm9naXdlYnVzZXI6cm9naXdlYnVzZXJAM2VtYmVkIw==');
				req.setHeader('Content-Type', 'application/json');
				req.setBody(jsonBody);
				Http http = new Http();
				HttpResponse res = http.send(req);
				System.debug(LoggingLevel.FINEST,'>>>>>>>>>>>>>>>>>>>>'+res);
        		System.debug(LoggingLevel.FINEST,'>>>>>>>>>>>>>>>>>>>>'+res.getBody());

		}
	
}